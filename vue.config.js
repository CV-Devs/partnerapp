
module.exports = {
    devServer: {
        proxy: 'https://staging-api.crowdyvest.com/api'
    },
    chainWebpack: config => {
        config.performance
            .maxEntrypointSize(40000000)
            .maxAssetSize(40000000)
    }
}