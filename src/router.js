import Vue from 'vue';
import VueRouter from 'vue-router';
import Routes from './routes';
import {getLoggedInUser} from './services/auth';

Vue.use(VueRouter);

const router = new VueRouter(Routes);

router.beforeEach((to, from, next) => {
    
    if (to.meta.requiresAuth && !getLoggedInUser()) {
        next({name: 'login'});
    }
    if (!to.meta.requiresAuth && getLoggedInUser()) {
        next({name: 'dashboard'});
    }
    next()
})

router.afterEach((to, from) => {
    if (to.meta.title) document.title = to.meta.title+' | '+process.env.VUE_APP_NAME;
})

export default router;
