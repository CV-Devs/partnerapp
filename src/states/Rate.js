import Axios from "axios";
import Http from "../services/http";
import { getApiUrl } from "../services/util";

export default {
    namespaced: true,
    
    state: {
        rates: [],
        ratesLoading: false,
        ratesError: null,

        statistics: [],
        statisticsLoading: false,
        statisticsError: null,
    },

    getters: {
        extractRates(state) {
            return state.rates.data;
        },

        getRatesMeta (state) {
            return state.rates.meta;
        },

        getRatesLinks (state) {
            return state.rates.links;
        },


        getStatisticsLabels(state) {
            return Object.keys(state.statistics.sell_rates);
        },

        getStatisticsSellRates(state) {
            return Object.values(state.statistics.sell_rates);
        },

        getStatisticsBuyRates (state) {
            return Object.values(state.statistics.buy_rates);
        },

    },

    mutations: {
        setRates(state, payload) {
            state.rates = payload;
        },

        setRatesLoading(state, payload) {
            state.ratesLoading = payload;
        },

        setRatesError(state, payload) {
            state.ratesError = payload;
        },


        setStatistics (state, payload) {
            state.statistics = payload;
        },

        setStatisticsLoading(state, payload) {
            state.statisticsLoading = payload;
        },

        setStatisticsError(state, payload) {
            state.statisticsError = payload;
        },
    },

    actions: {
        getRates({commit}, page = 1) {
            const endpoint = getApiUrl('/fx-rates/history?page='+page);

            commit('setRatesLoading', true);

            Http.get(endpoint)
                .then(({data}) => {
                    commit('setRates', data.data);
                })
                .catch((e) => { 
                    commit('setRatesError', e)
                })
                .finally(() => {
                    commit('setRatesLoading', false);
                });

            // Axios.get(endpoint, {params: {page}})
            //     .then(({data}) => {
            //         commit('setRates', data.data);
            //     })
            //     .catch((e) => { 
            //         commit('setRatesError', e.response)
            //     })
            //     .finally(() => {
            //         commit('setRatesLoading', false);
            //     });
        },

        getStatistics ({commit}) {
            const endpoint = getApiUrl('/fx-rates/statistics');

            commit('setStatisticsLoading', true);

            Http.get(endpoint)
                .then(({data}) => {
                    commit('setStatistics', data); 
                })
                .catch((e) => {
                    commit('setStatisticsError', e);
                })
                .finally(() => {
                    commit('setStatisticsLoading', false);
                })

            // Axios.get(endpoint)
            //     .then(({data}) => {
            //         commit('setStatistics', data); 
            //     })
            //     .catch((e) => {
            //         commit('setStatisticsError', e.response);
            //     })
            //     .finally(() => {
            //         commit('setStatisticsLoading', false);
            //     })
        },
    },
}