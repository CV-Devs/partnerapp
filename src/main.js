import Vue from 'vue'
import App from './views/App.vue'
import store from './store'
import router from './router'
import { getLoggedInUser } from './services/auth'

Vue.config.productionTip = false

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Authorization'] = `Bearer ${getLoggedInUser()}`;
window.axios.defaults.headers.common['Accept'] = 'application/json';
window.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';


new Vue({
  render: h => h(App),
  store,
  router,
}).$mount('#app')
