import Swal from "sweetalert2";

export function getUrlSegment(position) {
    const segments = window.location.href.split('/');
    return segments[position];
}

export function handleValidationError(response) {
    const {status} = response;
    if (status == 422 && response.data.data) {
        const {data} = response;
        const d = data.data;
        const error = d[Object.keys(d)[0]][0];
        Swal.fire('Oops!', error, 'error');
    } else if (status == 422 && response.data.message && !response.data.success) {
        Swal.fire('Oops!', response.data.message, 'error');
    }
}


export function getApiUrl(endpoint = null) {

    const baseUrl = process.env.VUE_APP_BASE_URL

    if (endpoint) {
        let url = endpoint
        const prefix = endpoint.substr(0, 1)

        if (prefix != '/') {
            url = '/'.endpoint
        }
        return  baseUrl+url
    }

    return baseUrl
}