
import Axios from 'axios';
import {getUserDetails} from '../services/auth';
import Http from '../services/http';
import { getApiUrl } from '../services/util';

export default {
    namespaced: true,

    state: {
        user: null,
        userLoading: false,
        userError: null,

        currentRate: null,
        currentRateLoading: false,
        currentRateError: null,
    },

    getters: {},

    mutations: {
        setUser(state, payload) {
            state.user = payload;
        },

        setUserLoading(state, payload) {
            state.userLoading = payload;
        },

        setUserError(state, payload) {
            state.userError = payload;
        },

        setCurrentRate(state, payload) {
            state.currentRate = payload;
        },
        setCurrentRateLoading (state, payload) {
            state.currentRateLoading = payload;
        },
        setCurrentRateError(state, payload) {
            state.currentRateError = payload;
        }
    },

    actions: {
        async getUser({commit}) {
            commit('setUserLoading', true);
            try {
                commit('setUser', await getUserDetails());
            } catch (error) {
                commit('setUserError', error);
            }
            commit('setUserLoading', false);
        },

        getCurrentRate({commit}) {
            const endpoint = getApiUrl('/fx-rate/usd');

            commit('setCurrentRateLoading', true);

            Http.get(endpoint)
                 .then(({data}) => {
                    commit('setCurrentRate', data.data);
                })
                .catch((error) => {
                    console.log(error)
                    commit('setCurrentRateError', error);
                })
                .finally(() => {
                    commit('setCurrentRateLoading', false);
                });

            // Axios.get(endpoint)
            //     .then(({data}) => {
            //         commit('setCurrentRate', data.data);
            //     })
            //     .catch((error) => {
            //         console.log(error)
            //         commit('setCurrentRateError', error);
            //     })
            //     .finally(() => {
            //         commit('setCurrentRateLoading', false);
            //     });
        },
    },
}