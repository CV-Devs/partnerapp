
import Vue from 'vue';
import Vuex from 'vuex';
import Rate from './states/Rate';
import Dashboard from './states/Dashboard';
import Transaction from './states/Transactions';


Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    modules: {
        Dashboard,
        Rate,
        Transaction,
    }
});