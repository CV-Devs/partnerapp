import LoginComponent from './views/auth/Login.vue';
import DashboardComponent from './views/dashboard/Index.vue';
import ForgotPasswordComponent from './views/auth/ForgotPassword';
import ResetPasswordComponent from './views/auth/ResetPassword.vue';
import FxRatesComponent from './views/fx-rates/Index.vue';
import CreateFxRateComponent from './views/fx-rates/Create.vue';
import TransactionsComponent from './views/transactions/Index.vue'


export default {
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: LoginComponent,
            meta: {
                title: 'Login',
                requiresAuth: false
            }
        },
        {
            path: '/forgot-password',
            name: 'forgot.password',
            component: ForgotPasswordComponent,
            meta: {
                title: 'Forgot Password',
                requiresAuth: false,
            }
        },
        {
            path: '/reset-password',
            name: 'reset.password',
            component: ResetPasswordComponent,
            meta: {
                title: 'Reset Password',
                requiresAuth: false,
            }
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: DashboardComponent,
            meta: {
                title: 'Dashboard',
                requiresAuth: true,
            }
        },
        {
            path: '/fx-rates',
            name: 'fx.rates',
            component: FxRatesComponent,
            meta: {
                title: 'Fx Rates',
                requiresAuth: true,
            }
        },
        {
            path: '/fx-rates/create',
            name: 'create.fx.rate',
            component: CreateFxRateComponent,
            meta: {
                title: 'New FX Rate',
                requiresAuth: true,
            }
        },
        {
            path: '/transactions',
            name: 'transactions',
            component: TransactionsComponent ,
            meta: {
                title: 'Transactions',
                requiresAuth: true,
            }
        }
    ],
};
