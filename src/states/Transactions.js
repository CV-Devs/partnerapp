import Axios from "axios";
import Http from "../services/http";
import {getApiUrl} from '../services/util'

export default {
    namespaced: true,

    state: {
        transactions: [],
        transactionsLoading: false,
        transactionsError: null,

        volume: null,
        volumeLoading: false,
        volumeError: null,
    },

    getters: {
        extractTransactions (state) {
            return state.transactions.data;
        },
        getTransactionsLinks (state) {
            return state.transactions.links;
        },
        getTransactionsMeta(state) {
            return state.transactions.meta;
        },
    },

    mutations: {
        setTransactions(state, payload) {
            state.transactions = payload;
        },

        setTransactionsLoading(state, payload) {
            state.transactionsLoading = payload;
        },

        setTransactionsError(state, payload) {
            state.transactionsError = payload;
        },

        setVolume(state, payload) {
            state.volume = payload;
        },

        setVolumeLoading(state, payload) {
            state.volumeLoading = payload;
        },

        setVolumeError(state, payload) {
            state.volumeError = payload;
        },
    },

    actions: {
        getTransactions({commit}, payload ={}) {
            const endpoint = getApiUrl('/fx-rates/transactions');
            
            commit('setTransactionsLoading', true);

            Http.get(endpoint , payload)
                .then(({data}) => {
                    commit('setTransactions', data.data);
                })
                .catch((e) => {
                    commit('setTransactionsError', e);
                })
                .finally(() => {
                    commit('setTransactionsLoading', false);
                })

            // Axios.get(endpoint, {params: {page, filter}})
            //     .then(({data}) => {
            //         commit('setTransactions', data.data);
            //     })
            //     .catch((e) => {
            //         commit('setTransactionsError', e.response);
            //     })
            //     .finally(() => {
            //         commit('setTransactionsLoading', false);
            //     })
        },

        getVolume({commit}) {
            const endpoint = getApiUrl('/fx-rates/transactions/today-volume');

            commit('setVolumeLoading', true);

            Http.get(endpoint)
                .then(({data}) => {
                    commit('setVolume', data.data);
                })
                .catch( (e) => {
                    commit('setVolumeError', e);
                })
                .finally(() => {
                    commit('setVolumeLoading', false);
                });

            // Axios.get(endpoint)
            //     .then(({data}) => {
            //         commit('setVolume', data.data);
            //     })
            //     .catch( (e) => {
            //         commit('setVolumeError', e.response);
            //     })
            //     .finally(() => {
            //         commit('setVolumeLoading', false);
            //     });
        },
    },
}