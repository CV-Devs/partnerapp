import Axios from "axios";
import http from "./http";
import { getApiUrl } from "./util";
import Crypto from 'crypto'

const payloadKey = '__fx_rate_user';
const codeKey = 'CV-PARTNER';

export  function getLoggedInUser() {
    const savedUser = localStorage.getItem(payloadKey);
    if (!savedUser) return null;
    return decodeData(JSON.parse(localStorage.getItem(payloadKey)));
}

export function saveLoggedInUser(payload) {
    const savedUser = localStorage.getItem(payloadKey);
    if (savedUser) localStorage.removeItem(payloadKey);

    return localStorage.setItem(payloadKey, JSON.stringify(encodeData(payload)));
}

export async function getUserDetails()
{
    const endpoint = getApiUrl('/profile');
    try {
        // const {data} = await Axios.get(endpoint);
        // return data.data.user;
        const {data} = await http.get(endpoint);
        return data.data.user;
    } catch (error) {
        return error;
    }
}

export async function logUserOut() {
    const endpoint = getApiUrl('/logout');

    try {
        const {data} = await Axios.post(endpoint);
        return data.data;
    } catch (error) {
        return error
    }
}

export function discardLoggedUserToken() {
    return localStorage.removeItem(payloadKey);
}

export function encodeData(payload) {
    let cipher = Crypto.createCipher('aes-256-ctr', codeKey)
    let crypted = cipher.update(payload, 'utf8', 'base64')
    crypted += cipher.final('base64')

    return crypted
}

export function decodeData(payload) {
    let cipher = Crypto.createDecipher('aes-256-ctr', codeKey)
    let crypted = cipher.update(payload, 'base64', 'utf8')
    crypted += cipher.final('utf8')

    return crypted
}