import Swal from 'sweetalert2';
import {getLoggedInUser} from './auth'

export default {
    getHeaders() {
        return {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getLoggedInUser()}`,
        }
    },

    getUrlParams(paramData) {
        return new URLSearchParams(paramData);
    },

    async call(url, method, data) {
        const response = await fetch(url, {
            body: JSON.stringify(data),
            method,
            headers: this.getHeaders(),
        })

        const respJson = response.json();

        // if (response.status == 422) {
        //     // console.log(response)
        //     // respJson.then(function(data) {
        //     //     const d = data.data;
        //     //     const error  = d[Object.keys(d)[0]][0];
        //     //     throw new Error(error)
        //     // })
        // }

        return new Promise((resolve) => respJson.then(data => resolve({
            status: response.status,
            statusText: response.statusText,
            data,
        })))
        
    },

    async get(url, paramData = null) {
        if (paramData)
            return this.call(url + '?' + this.getUrlParams(paramData), 'get')
        else
            return this.call(url)
    },

    async post(url, data) {
        return this.call(url, 'post', data)
    },

    async delete(url) {
        return this.call(url, 'delete')
    },

    async put(url, data) {
        return this.call(url, 'put', data)
    }
}